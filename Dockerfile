#stage 1
FROM alpine:latest AS builder
LABEL maintainer="ofekatr1el@gmail.com"
ENV NGINX_VERSION=1.18.0
#download nginx source code
RUN wget https://nginx.org/download/nginx-${NGINX_VERSION}.tar.gz && tar zxvf nginx-${NGINX_VERSION}.tar.gz
#install nginx dependencies
RUN apk add --update-cache \
    gcc \
    perl \
    linux-headers \
    build-base
WORKDIR /nginx-${NGINX_VERSION}
# download dependencies for configuration
RUN wget https://ftp.pcre.org/pub/pcre/pcre-8.40.tar.gz && tar xzvf pcre-8.40.tar.gz \
&& wget http://www.zlib.net/zlib-1.2.11.tar.gz && tar xzvf zlib-1.2.11.tar.gz \
&& wget https://www.openssl.org/source/openssl-1.1.1d.tar.gz && tar xzvf openssl-1.1.1d.tar.gz 
RUN rm -rf *.tar.gz
RUN ./configure \
    --with-http_ssl_module \
    --with-pcre=pcre-8.40 \
    --with-openssl=openssl-1.1.1d \
    --with-zlib=zlib-1.2.11 \
    --conf-path=/etc/nginx/nginx.conf \
    --sbin-path=/usr/local/bin/nginx \
    --error-log-path=/var/log/nginx/error.log \
    --pid-path=/var/run/nginx.pid \
    --http-log-path=/var/log/nginx/access.log \
    --with-http_v2_module \
    --http-client-body-temp-path=/var/cache/nginx/client_temp \
    && make install


FROM alpine:latest
COPY --from=builder /usr/local/bin/nginx /usr/local/bin
COPY ./nginx.conf /etc/nginx/nginx.conf
COPY ./index.html /ofek/test/

RUN apk add libcap\
    openssl
# setcap in order to use ports under 1024
RUN setcap 'cap_net_bind_service=+ep' /usr/local/bin/nginx
RUN openssl req -new -newkey rsa:4096 -days 365 -nodes -x509 \
    -subj "/C=IL/ST=Rishon/L=Israel/O=Ofek/CN=ofek.nginx" \
    -keyout /etc/ssl/private/nginx-selfsigned.key \
    -out /etc/ssl/certs/nginx-selfsigned.crt

# add user and group nginx and grant them ownership to required paths
RUN addgroup -S nginx && adduser -S nginx -G nginx \
&& mkdir /var/log/nginx && chown nginx:nginx /var/log/nginx \
&& mkdir /var/cache/nginx && chown nginx:nginx /var/cache/nginx \
&& mkdir /usr/local/nginx/ && chown nginx:nginx /usr/local/nginx \
&& chown nginx:nginx /var/run/ \
&& chown nginx:nginx /etc/ssl/private/nginx-selfsigned.key \
&& chown nginx:nginx /etc/ssl/certs/nginx-selfsigned.crt

USER nginx
CMD ["/usr/local/bin/nginx"]
