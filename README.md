# N-Exam

Ofek Katriel's Nemesis exam.

## Installation

use the 'playbook.yml' to install 'docker','docker-compose' and register a 'gitlab-runner' for this project in your linux server 

```bash
ansible-playbook playbook.yml
```

## Usage

build the dockerfile in it's current state with tag : 'nginx_test_ssl'

run docker-compose
```bash
docker build . -t nginx_test_ssl
docker-compose up -d
```

## Notes

the project was developed on azure's centos vm.
in order to view the html page, you will need to commit one of the following:
1. get Chrome to accept a self-signed certificate
2. comment/delete line 30 in 'nginx.conf' file (return 301 https://$host$request_uri;)

Thank you,

Ofek Katriel
